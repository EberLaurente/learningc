#include <stdio.h>
#include "./Headers/toBinomial.h"

int main(void)
{
  int b[32];
  for (int i=0; i < 32; i++){
    b[i] = 0;
  }

  unsigned x = 3252525;

  printf("Number %u as Binomial:", x);
  toBinomial(b, x);
  printToBinomial(b);


  return 0;
}
