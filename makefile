# Compiler

CC = gcc

# Executable

TARGET = main

# Compiler's flags
# -g: active debug functions
# -wall: actives compiler's cavens 

CFLAGS = -g -Wall

# Resources and codes

SRC = main.c src/toBinomial.c

# Executable command

all:
	$(CC) $(CFLAGS) $(SRC) -o ./Build/$(TARGET) && ./Build/$(TARGET)

clean:
	rm -rf ./Build/$(TARGET)
