#include <stdio.h>
#include "../Headers/toBinomial.h"

void toBinomial(int b[], unsigned num){
  for (int i=0; i < 32; i++){
    b[i] = 0;
  }

  int idx = 31;
  unsigned q = 0;
  unsigned d = num;
  while ((q = d / 2) > 1) {
    b[idx--] = d%2;
    d = q;
  }

  b[idx--] = d % 2;
  b[idx--] = d / 2;
}

unsigned pow2(int exp){
  unsigned result = 1;
  for (int i = 1; i <= exp; i++){
    result = result * 2;
  }
  return result;
}

void printToBinomial(int b[]){
  printf("\n\nThe binomial number is: \n");
  for (int i = 0; i < 32; i++)
    printf(" %d |", b[i]);
}

unsigned setBits(unsigned x, int p, int n, unsigned y){
  // mask of x
  int maskX = 0, maskY = 0;
  maskX = (~0 << (p + 1)) | ~(~0 << (p + 1 -n));
  x = x & maskX;

  maskY = ~(~0 << n);
  y = y & maskY;
  y = y << (p + 1 - n);

  x = x | y;

  return x;
}

unsigned invert(unsigned x, int p, int n){
  int mask1 = 0, mask2 = 0;
  unsigned x1;
  mask1 = ~(~0 << (p + 1)) & (~0 << (p + 1 - n));
  x1 = ~x & mask1;

  mask2 = (~0 << (p + 1)) | ~(~0 << (p + 1 - n));
  x = x & mask2;
  x = x | x1;
  return x;
}

unsigned rightRot(unsigned x, int n){
  unsigned rightSide = 0;
  unsigned leftSide = 0;
  rightSide = x << (n + 1);
  leftSide = x >> (32 - n - 1);
  x = rightSide | leftSide;
  return x;
}

