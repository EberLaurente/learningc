#ifndef TOBINOMIAL_H_INCLUDED
#define TOBINOMIAL_H_INCLUDED

void toBinomial(int b[], unsigned num);
unsigned pow2(int exp);
void printToBinomial(int b[]);
unsigned setBits(unsigned x, int p, int n, unsigned y);
unsigned invert(unsigned x, int p, int n);
unsigned rightRot(unsigned x, int n);


#endif /* TOBINOMIAL_H_INCLUDED */

